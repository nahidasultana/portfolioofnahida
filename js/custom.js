/* WINDOW LOAD EVENTS */

jQuery(window).load(function () {
    "use strict";
    jQuery("body").find('#site-loading').fadeOut(500);
});

/* SIDEBAR */

jQuery("body").find(".cv-menu-button").on("click", function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).toggleClass("rotate-menu-icon");
    jQuery("#cv-sidebar").toggleClass("open");
});

/* BACK TO TOP */

jQuery("#cv-back-to-top").on('click', function (event) {
    "use strict";
    event.preventDefault();
    jQuery('#cv-page-right').animate({
        scrollTop: 0
    }, 500);
});

/* OTHER EVENTS */

jQuery(document).ready(function () {
    "use strict";
    /* SIDEBAR CUSTOM SCROLLBAR */
    if (jQuery(window).width() > 1024) {
        jQuery("#cv-sidebar-inner").mCustomScrollbar({
            scrollInertia: 500,
            autoHideScrollbar: true,
            theme: "light-thick",
            scrollButtons: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true
            }
        });
    }

    /* DON'T ACTIVATE TOOLTIPS ON MOBILE DEVICES */
    if (jQuery(window).width() > 1024) {

        /* MENU TOOLTIP */
        jQuery("body").find('.tooltip-menu').tooltipster({
            theme: 'tooltipster-dark',
            delay: 0,
            hideOnClick: true,
            touchDevices: false,
            position: 'right',
            animation: 'swing'
        });

        /* GO TO TOP TOOLTIP */
        jQuery("body").find('.tooltip-gototop').tooltipster({
            theme: 'tooltipster-gototop',
            delay: 0,
            hideOnClick: true,
            touchDevices: false,
            position: 'top',
            animation: 'grow'
        });

    }

});