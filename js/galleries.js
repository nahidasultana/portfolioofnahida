// GALLERY 1
jQuery('#gallery1').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/cover-page.jpg',
            'thumb': 'images/portfolio/cover-page-small.jpg',
            'subHtml': 'This book cover page doesn’t belong any one of my course projects. I created this cover page from my own idea.'
        },{
            'src': 'images/portfolio/book.jpg',
            'thumb': 'images/portfolio/book-poster.jpg',
            'subHtml': 'This is a Book cover page that was created as the midterm project in my CIST1530- Web Graphics I class.'
        }]
    });
});

// GALLERY 2
jQuery('#gallery2').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/circus.jpg',
            'thumb': 'images/portfolio/circus-small.jpg',
            'subHtml': 'Lorem ipsum dolor'
        }]
    });
});

// GALLERY 3
jQuery('#gallery3').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/movie.jpg',
            'thumb': 'images/portfolio/movie-poster.jpg',
            'subHtml': 'Quae expetendis'
        }]
    })
});

// GALLERY 4
jQuery('#gallery4').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/book.jpg',
            'thumb': 'images/portfolio/book-poster.jpg',
            'subHtml': 'This is a Book cover page that was created as the midterm project in my CIST1530- Web Graphics I class.'
        },{
            'src': 'images/portfolio/cover-page.jpg',
            'thumb': 'images/portfolio/cover-page-small.jpg',
            'subHtml': 'This book cover page doesn’t belong any one of my course projects. I created this cover page from my own idea.'
        }]
    });
});
// GALLERY 5
jQuery('#gallery5').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/business-logo.png',
            'thumb': 'images/portfolio/business-logo-small.png',
            'subHtml': 'I have made this business logo using photoshop.'
        },{
            'src': 'images/portfolio/clyde-logo.jpg',
            'thumb': 'images/portfolio/clyde-logo.jpg',
            'subHtml': 'This logo was created as a midterm project for one of my web graphics class.'
        }]
    });
});
// GALLERY 6
jQuery('#gallery6').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/flyer-move.png',
            'thumb': 'images/portfolio/flyer-move-small.png',
            'subHtml': 'I made this design using Adobe Photoshop.'
        }]
    });
});
// GALLERY 7
jQuery('#gallery7').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/brochure-front.jpg',
            'thumb': 'images/portfolio/brochure-front-small.jpg',
            'subHtml': 'This brochure was created as a final project for one of my class.'
        },{
            'src': 'images/portfolio/brochureback.jpg',
            'thumb': 'images/portfolio/brochure-front-small.jpg',
            'subHtml': 'This brochure was created as a final project for one of my class.'
        }]
    });
});
// GALLERY 8
jQuery('#gallery8').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/t-shirt-design1-lg.png',
            'thumb': 'images/portfolio/t-shirt-design1.png',
            'subHtml': 'I made this design using Adobe Photoshop and Illustrator.'
        },{
            'src': 'images/portfolio/t-shirt-design9.png',
            'thumb': 'images/portfolio/t-shirt-design9-small.png',
            'subHtml': 'I made this design using Adobe Photoshop and Illustrator.'
        },{
            'src': 'images/portfolio/t-shirt-design8-lg.png',
            'thumb': 'images/portfolio/t-shirt-design8.png',
            'subHtml': 'I made this design using Adobe Photoshop and Illustrator.'
        },{
            'src': 'images/portfolio/t-shirt-design4.png',
            'thumb': 'images/portfolio/t-shirt-design4-small.png',
            'subHtml': 'I made this design using Adobe Photoshop.'
        }]
    });
});
// GALLERY 9
jQuery('#gallery9').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/photography-flyer.png',
            'thumb': 'images/portfolio/photography-flyer-small.png',
            'subHtml': 'I made this design using Adobe Photoshop.'
        }]
    });
});
// GALLERY 10
jQuery('#gallery10').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/clyde-logo.jpg',
            'thumb': 'images/portfolio/clyde-logo.jpg',
            'subHtml': 'This logo was created as a midterm project for one of my web graphics class.'
        },{
            'src': 'images/portfolio/business-logo.png',
            'thumb': 'images/portfolio/business-logo-small.png',
            'subHtml': 'I have made this business logo using photoshop.'
        }]
    });
});
// GALLERY 11
jQuery('#gallery11').on('click', function (e) {
    "use strict";
    e.preventDefault();
    jQuery(this).lightGallery({
        // Settings
        dynamic: true,
        mode: 'lg-slide-vertical',
        zoom: true,
        fullScreen: true,
        autoplay: false,
        thumbnail: true,
        download: true,
        counter: true,
        // Images
        dynamicEl: [{
            'src': 'images/portfolio/cooking-banner.png',
            'thumb': 'images/portfolio/cooking-banner.png',
            'subHtml': 'I made this design using Adobe Photoshop.'
        }]
    });
});