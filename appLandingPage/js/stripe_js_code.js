$(document).ready(function () {

	$('.support-monthly-gift').click(function () {
		$('.one-time-check').hide();
		$('.monthly_support_check').show();
		$('#actions').val('subscription');
	});

	$('.one-time-gift').click(function () {
		$('.monthly_support_check').hide();
		$('.one-time-check').show();
		$('#actions').val('onetimepayment');
	});

	$('.show_popup1').click(function () {
		var first_input = $('.modal_amount').val();
		$('.donate_amount').val(first_input);
	});

	/*  var handler = StripeCheckout.configure({
	 key: 'pk_live_jU8vp8HQh6QmL84mwStdfPwm',
	 image: '',
	 locale: 'auto',
	 token: function(token) {
	 // ajax call
	 var action = $('#actions').val();
	 var donate_amount = $('.donate_amount').val();
	 $.ajax({
	 url: "http://coogodslove.com/charge_donate_customer.php",
	 type: "POST",
	 dataType:"json",
	 data: { token_id: token.id, action: action, amount: donate_amount, email: token.email },
	 success: function(data) {
	 }
	 });

	 // You can access the token ID with `token.id`.
	 // Get the token ID to your server-side code for use.
	 }
	 });*/

	var handler = StripeCheckout.configure({
		key: 'pk_live_jU8vp8HQh6QmL84mwStdfPwm',
		image: '',
		token: function (token) {
			$("#stripeToken").val(token.id);
			$("#stripeEmail").val(token.email);
			$("#myForm").submit();
		}
	});


	var amount = $('.donate_amount').val();

	$('.donate_button').on('click', function (e) {
		if ($('.donate_amount').val() == '') {
			alert('Please enter amount.');
			return;
		}
		var description = ( $('#actions').val() == 'subscription') ? 'Monthly Plan' : 'One Time Payment';
		// Open Checkout with further options:
		handler.open({
			name: 'COO APP PROJECT',
			description: description,
			currency: "USD",
			amount: amount * 100,
			zipCode: true,
			billingAddress: true,
			panelLabel: 'Donate'
    });
		e.preventDefault();
	});

	$('.paypal_donate_button').on('click', function (e) {
		var amount = $('.donate_amount').val();
		if ($('.donate_amount').val() == '') {
			alert('Please enter amount.');
			return;
		}
		var description = ( $('#actions').val() == 'subscription') ? 'Monthly Plan' : 'One Time Payment';
		// Open Checkout with further options:
		if (description == "One Time Payment") {
			window.location.href = "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=holydovemediaministry@gmail.com&currency_code=USD&item_name=COO APP PROJECT&amount=" + amount + "&return=http://coogodsloveproject.com&cancel_return=http://coogodsloveproject.com";
		} else {
			window.location.href = "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick-subscriptions&business=holydovemediaministry@gmail.com&currency_code=USD&item_name=COO APP PROJECT&a3=" + amount + "&p3=1&t3=M&src=1&return=http://coogodsloveproject.com&cancel_return=http://coogodsloveproject.com";
		}


		e.preventDefault();
	});

	// Close Checkout on page navigation:
	// $(window).on('popstate', function() {
	//   handler.close();
	// });
});
