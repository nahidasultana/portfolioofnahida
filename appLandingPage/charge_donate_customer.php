<?php
try {
    require_once('Stripe/lib/Stripe.php');
    Stripe::setApiKey("sk_live_Her2TJn1JlPu3CXCDkYE1OgU");
    if (isset($_POST['donate_amount'])) {

        $amount = $_POST['donate_amount'];

        if ($_POST['actions'] == 'subscription') {
            $plan_id = 'plan_' . $amount;
            $create_new_plan = 'yes';
            try {
                $is_plan_exist = Stripe_Plan::retrieve($plan_id);
                $create_new_plan = 'no';
            } catch (Exception $e) {
                //echo "PLan not exist and created new plan";
            }

            if ($create_new_plan == 'yes') {
                $plan = Stripe_plan::create(array(
                    "amount" => $amount * 100,
                    "interval" => "month",
                    "name" => "New plan created",
                    "currency" => "USD",
                    "id" => $plan_id
                ));
            }

            $customer = Stripe_Customer::create(array(
                'source' => $_POST['stripeToken'],
                'email' => $_POST['stripeEmail'],
                'plan' => $plan_id,
                "description" => "Created new monthly customer"
            ));

            echo '<script type="text/javascript">alert("Thank you for your payment, we will be in touch soon");
     window.location=index.php;
    </script>';

        } else if ($_POST['actions'] == 'onetimepayment') {

            $charge = Stripe_Charge::create(array(
                "amount" => $amount * 100,
                "currency" => "USD",
                "card" => $_POST['stripeToken'],
                "receipt_email" => $_POST['stripeEmail'],
                "description" => "Successfully completed online payment."
            ));

            echo '<script type="text/javascript">alert("Thank you for your payment, we will be in touch soon");
     window.location=index1.php;
    </script>';
        }

    }
} catch (Stripe_CardError $e) {
    $e_json = $e->getJsonBody();
    $error = $e_json['error'];

    echo '<script type="text/javascript">alert("There is an issue, payment fail");
     window.location="http://coogodsloveproject.com/index.php";
    </script>';
}


?>